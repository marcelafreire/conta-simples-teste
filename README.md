
## Instalação

 - Faça o download do repositório.
 - Use o gerenciador de pacotes npm.
 - Dentro da pasta do projeto execute npm install no terminal para instalar todas as dependências.

```
npm install
```

## Instruções

- Para rodar a aplicação, execute npm start

```
npm start
```


- Para rodar a API, Após inicializar a aplicação, abra um segunda janela no terminal e execute  JSON Server Auth

```
json-server -p 4000 db.json
```

## Sobre o projeto
- Para a API fake foi utilizado o JSON server.
- No desenvolvimento do front-End, foi utiltilizado o ReactJs, HTML5 e CSS3.
- Para chamadas da API no front-end, utilizou-se axios.
